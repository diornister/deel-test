const { Sequelize } = require("sequelize")

const payJob = async (req, res) => {
  const { Profile, Job, Contract } = req.app.get("models")
  const { id } = req.params

  const job = await Job.findOne({
    where: { id },
    include: [Contract],
  })

  const { ContractorId, ClientId } = job.Contract

  const contractor = await Profile.findOne({
    where: {
      id: ContractorId,
    },
  })

  if (job.price > contractor.balance)
    return res.json({ msg: "Insuficient Balance" })

  try {
    await Sequelize.transaction(async (transaction) => {
      const client = Profile.findOne({
        where: {
          id: ClientId,
        },
      })

      Profile.update(
        {
          balance: client.balance + job.price,
        },
        {
          where: {
            id: ClientId,
          },
        },
        { transaction }
      )

      Profile.update(
        {
          balance: contractor.balance - job.price,
        },
        {
          where: {
            id: ContractorId,
          },
        },
        { transaction }
      )
    })

    res.json({ job })
  } catch (error) {
    res.json({ msg: "Transaction error" })
  }
}

module.exports = {
  payJob,
}
