const { Sequelize } = require("sequelize")

const getUnpaidActiveJobs = async (req, res) => {
  const { Profile, Job, Contract } = req.app.get("models")

  const jobs = await Profile.findAll({
    where: {
      id: req.profile.id,
    },
    include: [
      {
        model: Contract,
        as: "Contractor",
        where: {
          status: { [Sequelize.Op.ne]: "terminated" },
        },
        include: [
          {
            model: Job,
          },
        ],
      },
    ],
  })

  res.json(jobs)
}

module.exports = {
  getUnpaidActiveJobs,
}
