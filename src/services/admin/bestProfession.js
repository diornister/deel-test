const Sequelize = require("sequelize")

const bestProfession = async (req, res) => {
  const { Profile, Contract, Job } = req.app.get("models")
  const { start, end } = req.query

  const data = await Job.findAll({
    attributes: [[Sequelize.fn("sum", Sequelize.col("price")), "price_total"]],
    where: {
      paymentDate: {
        [Sequelize.Op.between]: [start, end],
      },
    },
    include: [
      {
        model: Contract,
        attributes: ["id"],
        include: [
          {
            model: Profile,
            attributes: ["profession"],
            as: "Client",
          },
        ],
      },
    ],
    group: [Sequelize.col("profession")],
    order: [[Sequelize.col("price_total"), "DESC"]],
    limit: 1,
  })

  res.json(data)
}

module.exports = bestProfession
