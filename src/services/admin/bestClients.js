const Sequelize = require("sequelize")

const bestClients = async (req, res) => {
  const { Profile, Contract, Job } = req.app.get("models")
  const { start, end, limit = 2 } = req.query
  try {
    const data = await Job.findAll({
      attributes: [[Sequelize.fn("sum", Sequelize.col("price")), "paid"]],
      where: {
        paymentDate: {
          [Sequelize.Op.between]: [start, end],
        },
      },
      include: [
        {
          model: Contract,
          attributes: ["id"],
          include: [
            {
              model: Profile,
              attributes: [
                [Sequelize.literal("firstName || ' ' || lastName"), "fullName"],
              ],
              as: "Contractor",
            },
          ],
        },
      ],
      order: [[Sequelize.col("paid"), "DESC"]],
      limit,
    })

    res.json(data[0])
  } catch (error) {
    console.log(error)
  }
}

module.exports = bestClients
