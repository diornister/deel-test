const getContracts = async (req, res) => {
  const { Contract } = req.app.get("models")

  const contracts = await Contract.findAll({
    where: { clientId: req.profile.id },
  })

  res.json(contracts)
}

module.exports = {
  getContracts,
}
