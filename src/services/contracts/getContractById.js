const getContractById = async (req, res) => {
  const { Contract } = req.app.get("models")
  const { id } = req.params
  const contract = await Contract.findOne({
    where: { id, clientId: req.profile.id },
  })
  if (!contract) return res.status(404).end()
  res.json(contract)
}

module.exports = getContractById
