const deposit = async (req, res) => {
  const { Profile, Contract, Job } = req.app.get("models")
  const { id } = req.params
  const { id: profileId } = req.profile
  const { amount } = req.body

  const jobs = await Job.findAll({
    where: {
      paid: false,
    },
    include: [
      {
        model: Contract,
        where: {
          ContractorId: profileId,
          status: "in_progress",
        },
      },
    ],
  })

  if (!jobs.length) return res.json({ msg: "Cant transfer balance" })

  const total = jobs.map((job) => job.price).reduce((sum, val) => sum + val)

  if (amount > total * 0.25)
    return res.json({ msg: "Cant transfer more then 25% of total jobs" })

  const user = await Profile.findOne({
    where: {
      id,
    },
  })

  const updatedUser = await Profile.update(
    {
      balance: user.balance + amount,
    },
    {
      where: {
        id,
      },
    }
  )

  res.json("success")
}

module.exports = deposit
