const express = require("express")
const bodyParser = require("body-parser")
const { sequelize } = require("./model")
const { Routes } = require("./controllers")
const { AdminRoutes } = require("./controllers/admin")

const app = express()

app.use(bodyParser.json())
app.set("sequelize", sequelize)
app.set("models", sequelize.models)

Routes(app)
AdminRoutes(app)
module.exports = app
