const { Router } = require("express")
const { getProfile } = require("../middleware/getProfile")
const { getUnpaidActiveJobs } = require("../services/jobs/getUnpaidActiveJobs")
const { payJob } = require("../services/jobs/payJob")

const jobsRoutes = Router()

// GET
jobsRoutes.get("/unpaid", getProfile, getUnpaidActiveJobs)

// POST
jobsRoutes.post("/:id/pay", getProfile, payJob)

module.exports = jobsRoutes
