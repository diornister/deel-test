const contractsRoutes = require("./contracts.controller")
const jobsRoutes = require("./jobs.controller")
const balancesRoutes = require("./balances.controller")

const Routes = (app) => {
  app.use("/contracts", contractsRoutes)
  app.use("/jobs", jobsRoutes)
  app.use("/balances", balancesRoutes)
}

module.exports = { Routes }
