const { Router } = require("express")
const { getProfile } = require("../middleware/getProfile")
const { getContracts } = require("../services/contracts/getContracts")
const getContractById = require("../services/contracts/getContractById")

const contractsRoutes = Router()

// GET
contractsRoutes.get("", getProfile, getContracts)
contractsRoutes.get("/:id", getProfile, getContractById)

module.exports = contractsRoutes
