const { Router } = require("express")
const { getProfile } = require("../middleware/getProfile")
const deposit = require("../services/balance/deposit")

const balancesRoutes = Router()

// POST
balancesRoutes.post("/deposit/:id", getProfile, deposit)

module.exports = balancesRoutes
