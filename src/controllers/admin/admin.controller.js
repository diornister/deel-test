const { Router } = require("express")
const { getProfile } = require("../../middleware/getProfile")
const bestProfession = require("../../services/admin/bestProfession")
const bestClients = require("../../services/admin/bestClients")

const adminRoutes = Router()

// GET
adminRoutes.get("/best-profession", getProfile, bestProfession)
adminRoutes.get("/best-clients", getProfile, bestClients)

module.exports = adminRoutes
