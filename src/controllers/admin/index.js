const adminRoutes = require("./admin.controller")

const AdminRoutes = (app) => {
  app.use("/admin", adminRoutes)
}

module.exports = { AdminRoutes }
